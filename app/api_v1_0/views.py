from django.db.utils import DatabaseError, IntegrityError
from django.core.exceptions import ObjectDoesNotExist
from rest_framework import status, permissions
from rest_framework.parsers import JSONParser
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.views import APIView

from cities import models
from cities import serializers as serial
from cities.utils import norm_city_name, form_json


SHOW_RESPONSE = True

VALID_SEARCH_TYPES = ['exact', 'iexact', 'contains', 'icontains', 'startswith', 'istartswith']


# ===============================================================================
#  Customized JSON renderer
# ===============================================================================
class UTF8JSONRenderer(JSONRenderer):
    """ Customized JSON renderer with UTF-8 charset """
    charset = 'utf8'


class States(APIView):
    """ State collection services. """

    parser_classes = (JSONParser,)
    renderer_classes = (UTF8JSONRenderer,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request):
        """ GET a list of states. """
        states = models.State.objects.all()
        serialized_states = serial.StateSerializer(states, many=True)
        SHOW_RESPONSE and print('############ Response:\n{}'.format(form_json(serialized_states.data)))
        return Response(serialized_states.data)


class StateName(APIView):
    """ Specific state service by name. """

    parser_classes = (JSONParser,)
    renderer_classes = (UTF8JSONRenderer,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request):
        """ GET a specific state by name or uf. """
        query_state_serializer = serial.QueryStateSerializer(data=request.data)
        if query_state_serializer.is_valid():
            serialized_state_query = query_state_serializer.data
            if not serialized_state_query:
                return Response({'error': 'At least one parameter is needed.'}, status=status.HTTP_400_BAD_REQUEST)
            try:
                state = models.State.objects.get(**serialized_state_query)
            except ObjectDoesNotExist as e:
                return Response({'error': 'State [{}] does not exists.'.format(serialized_state_query)},
                                status=status.HTTP_400_BAD_REQUEST)
            serialized_response = serial.StateSerializer(state).data
            SHOW_RESPONSE and print('############ Response:\n{}'.format(form_json(serialized_response)))
            return Response(serialized_response)
        return Response({'error': 'Invalid parameters: {}'.format(query_state_serializer.errors)},
                        status=status.HTTP_400_BAD_REQUEST)


class CityId(APIView):
    """ Specific city services by Id. """

    parser_classes = (JSONParser,)
    renderer_classes = (UTF8JSONRenderer,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, city_id):
        """ GET a specific city by city_id. """
        city = None
        try:
            city = models.City.objects.get(pk=city_id)
        except ObjectDoesNotExist as e:
            return Response({'error': 'Could not found city [{}].'.format(city_id)}, status.HTTP_404_NOT_FOUND)
        city_serializer = serial.CitySerializer(city)
        SHOW_RESPONSE and print('############ Response:\n{}'.format(form_json(city_serializer.data)))
        return Response(city_serializer.data)

    def delete(self, request, city_id):
        """ DELETE a specific city by city_id. """
        city = None
        try:
            city = models.City.objects.get(pk=city_id)
        except ObjectDoesNotExist as e:
            return Response({'error': 'Could not found city [{}].'.format(city_id)}, status.HTTP_404_NOT_FOUND)
        city_serializer = serial.CitySerializer(city)
        SHOW_RESPONSE and print('############ Response:\n{}'.format(form_json(city_serializer.data)))
        city.delete()
        return Response(city_serializer.data)


class CityName(APIView):
    """ Specific city services by name. """

    parser_classes = (JSONParser,)
    renderer_classes = (UTF8JSONRenderer,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request):
        """ GET a specific city by name. """
        city_name = ''
        city_uf = ''
        search_type = 'iexact'
        if 'name' in request.query_params:
            city_name = request.query_params['name']
        if not city_name:
            return Response({'error': 'Name is required.'}, status=status.HTTP_400_BAD_REQUEST)
        if 'uf' in request.query_params:
            city_uf = request.query_params['uf']
        if not city_uf:
            return Response({'error': 'UF is required.'}, status=status.HTTP_400_BAD_REQUEST)
        if 'search_type' in request.query_params:
            search_type = request.query_params['search_type']
        if search_type not in VALID_SEARCH_TYPES:
            return Response({'error': 'Invalid search type.'}, status=status.HTTP_400_BAD_REQUEST)
        try:
            state = models.State.objects.get(uf=city_uf)
        except ObjectDoesNotExist as e:
            return Response({'error': 'Could not found state [{}].'.format(city_uf)}, status.HTTP_404_NOT_FOUND)
        try:
            city_norm_name = norm_city_name(city_name)
            cities = models.City.objects.filter(**{'norm_name__{}'.format(search_type): city_norm_name}).filter(
                state__uf=city_uf)
        except DatabaseError as e:
            return Response({'error': 'Error trying to get the city(es): {}'.format(e)},
                            status=status.HTTP_400_BAD_REQUEST)
        serialized_response = serial.CitySerializer(cities, many=True).data
        SHOW_RESPONSE and print('############ Response:\n{}'.format(form_json(serialized_response)))
        return Response(serialized_response)

    def post(self, request):
        """ Insert a new city by name. """
        query_city_serializer = serial.QueryCitySerializer(data=request.data)
        if query_city_serializer.is_valid():
            serialized_query_city = query_city_serializer.data
            try:
                state = models.State.objects.get(uf=serialized_query_city['uf'])
            except ObjectDoesNotExist as e:
                return Response({'error': 'State does not exists: [{}]'.format(serialized_query_city['uf'])})
            city = models.City(name=serialized_query_city['name'], state=state)
            try:
                city.save()
            except IntegrityError as e:
                return Response({'error': 'Integrity error: {}'.format(e)}, status=status.HTTP_400_BAD_REQUEST)
            SHOW_RESPONSE and print('############ Response:\n{}'.format(form_json(serial.CitySerializer(city).data)))
            return Response(serial.CitySerializer(city).data, status=status.HTTP_201_CREATED)
        return Response({'error': 'Invalid parameters: {}'.format(query_city_serializer.errors)},
                        status=status.HTTP_400_BAD_REQUEST)
