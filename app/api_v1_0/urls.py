"""cities URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path

from api_v1_0 import views

app_name = 'api_v1_0'
urlpatterns = [
    path('states/', views.States.as_view(), name='states'),
    path('state/', views.StateName.as_view(), name='state_name'),
    path('city/<int:city_id>/', views.CityId.as_view(), name='city_id'),
    path('city/', views.CityName.as_view(), name='city_name'),
]
