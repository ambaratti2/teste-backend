from django.db import models

from cities.utils import norm_spaces, norm_city_name


class State(models.Model):
    """ Class representing a state of union. """
    max_size = {'name': 30, 'uf': 2}
    name = models.CharField(max_length=max_size['name'], unique=True)
    uf = models.CharField(max_length=max_size['uf'], unique=True)

    def __str__(self):
        return '{} ({})'.format(self.name, self.uf)


class City(models.Model):
    """ Class representing a city of a State. """
    max_size = {'name': 128, 'norm_name': 128}
    name = models.CharField(max_length=max_size['name'])
    norm_name = models.CharField(max_length=max_size['norm_name'])
    state = models.ForeignKey(State, on_delete=models.CASCADE)

    class Meta:
        unique_together = ['norm_name', 'state']
        ordering = ['name', 'state']
        indexes = [models.Index(fields=['norm_name', 'state'], name='city_state_idx')]

    def __str__(self):
        return '{} [{}] ({})'.format(self.name, self.norm_name, self.state.uf)

    def save(self, *args, **kwargs):
        """ Prepare information to be saved. """
        self.norm_name = norm_city_name(self.name)
        self.name = norm_spaces(self.name)
        super().save(*args, **kwargs)
