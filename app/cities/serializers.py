from rest_framework import serializers

from cities import models


class StateSerializer(serializers.ModelSerializer):
    """ State model serializer. """

    class Meta:
        model = models.State
        fields = ('id', 'name', 'uf')
        read_only_fields = ('id', 'name', 'uf')


class QueryStateSerializer(serializers.Serializer):
    """ State serializer for query parameters. """
    name = serializers.CharField(required=False)
    uf = serializers.CharField(required=False)


class CitySerializer(serializers.ModelSerializer):
    """ City model serializer. """

    class Meta:
        model = models.City
        fields = ('id', 'name', 'norm_name', 'state')
        read_only_fields = ('id', 'norm_name',)


class QueryCitySerializer(serializers.Serializer):
    """ City serializer for query parameters. """
    name = serializers.CharField()
    uf = serializers.CharField()
