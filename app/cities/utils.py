from functools import reduce
import json
import string as string_data
from unicodedata import normalize


def remove_punctuation(string, fill_char=''):
    """ Removes punctuation from string. """
    if string is not None:
        return reduce(lambda str_acc, ch: str.replace(str_acc, ch, fill_char),
                      list(string_data.punctuation),
                      string
                      ).strip()


def remove_accents(text):
    """ Remove accents of text. """
    return normalize('NFKD', text).encode('ASCII', 'ignore').decode()


def norm_spaces(text, fill_char=' '):
    """ Normalizes separators between words as fill_char. """
    return fill_char.join(text.split())


def norm_city_name(city_name):
    """ Return a city name normalized to lowercase, without special caracters
        or accents, with space normalization. """
    if city_name is None:
        city_name = ''
    return remove_accents(norm_spaces(remove_punctuation(city_name).lower()))


def form_json(data):
    """ Formats data as pretty printed json. """
    return json.dumps(data, indent=4, ensure_ascii=False, sort_keys=True)
