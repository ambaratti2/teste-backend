# Squad

## Cities API

### Sumary:

This is a test API for a cities and states database management.

### System Configuration:

The system is extensively configurated through the use of environment variables.

#### Web server environment variables:

The variables below, set on docker environment through the file `docker-composer.yaml` defines the web server configuration:

- **DJANGO_HOSTNAME**: Web server hostname.
- **DJANGO_HOSTNAME**: Web server hostname.
- **DJANGO_ALLOWED_HOSTS**: Hosts allowed to connect to the web server.
- **DJANGO_SECRET_KEY**: Used by Django to provide cryptographic signing.

- **DJANGO_DB_HOSTNAME**: Database hostname to be used by the web server.
- **DJANGO_DB_PORT**: Database port to be used by the web server.
- **DJANGO_DB_DATABASE**: Project's database name to be used by the web server.
- **DJANGO_DB_USER**: Database username.
- **DJANGO_DB_PASSWORD**: Database password.

- **DJANGO_ENVIRONMENT**: Indication of wich environment is being used by the web server (`development` or `production`). It's intended to be used with scripts that can read it's value and set the other environment variables accordingly.


#### Database server environment variables:

The variables below, set on docker environment through the file `docker-composer.yaml` defines the database server configuration:

- **POSTGRES_DB**: Database used by the database server to this project.
- **POSTGRES_USER**: Database server username.
- **POSTGRES_PASSWORD**: Database server password.

#### Functional tests system (Tavern):

The variables below specify how to access the web service and its credentials.

They must be set on the environment in wich the test are run.

- **DJANGO_PROTOCOL**: Wich web protocol the server is offering (`http` or `https`).
- **DJANGO_HOSTNAME**: Hostname of the web server (as seen by the test environment).
- **DJANGO_PORT**: Port used by the web server (as seen by the test environment).
- **DJANGO_ADMIN_USER**: Django admin user used as authentication credential to access the web services.
- **DJANGO_ADMIN_PASSWD**: Django admin password used as authentication credential to access the web services.


### API documentation:

The documentation below use the following conventions:

- **Url** is the way to access the endpoint. As the exact URL depends if you're accessing **development** or **production** environment, the exact scheme (http or https) and hostname are replaced for **<host>** here.
- **Input parameters** can be JSON in data body or query string parameters. This is indicated on the parameters specification.
- On **Output layout** there is an example illustrating the JSON response data. Please note the elipsis on some results, that means **repetition of blocks**.
- **Output status** show the status code expected when the request was successful (for border cases, please see testing code).


#### List of all available states:

- **Url**: <host>/api_v1_0/states/
- **Method**: GET
- **Input parameters**: none
- **Output layout**:
```json
    [
        {
            "id": 1,
            "name": "Acre",
            "uf": "AC"
        },
        {
            "id": 2,
            "name": "Alagoas",
            "uf": "AL"
        },
        ...
    ]
```
- **Output status**:    200


#### Retrieve a specific state by name or uf:

- **Url**: <host>/api_v1_0/state/
- **Method**: GET
- **Input parameters (data body)**:
    - name:     State name (optional, but one of these parameters must be present)
    - uf:       State UF (optional, but one of these parameters must be present)
- **Output layout**:
```json
    {
        "id":   1,
        "name": "Acre",
        "uf":   "AC"
    }
```
- **Output status**:    200


#### Retrieve a specific city by ID:

- **Url**: <host>/api_v1_0/city/<city id>/
- **Method**: GET
- **Output layout**:
```json
    {
        "id":   1,
        "name": "São Paulo",
        "norm_name":  "sao paulo"
    }
```
- **Output status**:    200
- **Notes**:

    <city id> is an integer
    norm_name is the normalized (lower cased, without accents or special characters, space normalized) name of the city


#### Retrieve a specific city (or cities) by name:

- **Url**: <host>/api_v1_0/city/
- **Method**: GET
- **Input parameters (data body)**:
    - name:     State name (required)
    - uf:       State UF (required)
    - search_type:  (see **Notes** below, default: iexact)
- **Output layout**:
```json

    [
        {
            "id":   1,
            "name": "São Paulo",
            "norm_name":  "sao paulo"
        },
        ...
    ]
```
- **Output status**:    200
- **Notes**:

    The retrieval will be made on the **normalized** value of name, so it disregards the name case, accenting, spaces or special characters.
    You can have the same city name but on different states (city / state pair must be unique). That's why uf is a required parameter.

    You can choose the **search_type** between:

- **exact**:    the exact name of a city
- **iexact**:   the exact case insensitive name of a city (this is the default search type)
- **contains**: cities that contains the searched name
- **icontains**:    cities that contains the searched name case insensitive
- **startswith**:   cities whose name starts with the searched name
- **istartswith**:  cities whose name starts with the searched name case insensitive


#### Create a new city:

- **Url**: <host>/api_v1_0/city/
- **Method**: POST
- **Input parameters (data body)**:
    - name:     State name (required)
    - uf:       State UF (required)
- **Output layout**:
```json
    {
        "id":   1,
        "name": "São Paulo",
        "norm_name":  "sao paulo"
    }
```
- **Output status**:    201
- **Notes**:

    The city name will be space normalized (to avoid insertion of similar names varying only by spaces. ex: `São Paulo` and `São` `  ` `Paulo`)


#### Delete a specific city by ID:

- **Url**: <host>/api_v1_0/city/<city id>/
- **Method**: DELETE
- **Output layout**:
```json
    {
        "id":   1,
        "name": "São Paulo",
        "norm_name":  "sao paulo"
    }
```
- **Output status**:    200
- **Notes**:

    <city id> is an integer
    norm_name is the normalized (lower cased, without accents or special characters, space normalized) name of the city
    Deleting is made only by ID to avoid mistakes.


### Functional tests:

Functional API testing is implemented through [Tavern](https://taverntesting.github.io/).

To execute the tests you need to be on a virtual environment that had installed requirements.txt (or at least tavern and it's dependencies).

The API uses basic authentication, and so the Tavern tests will extract credencials from the environment variables as explained on `System Configuration` topic.

These are the credentials of Django admin page and are already included on the docker build database through Django fixtures.
Initialy they are:

- **DJANGO_ADMIN_USER**: cities
- **DJANGO_ADMIN_PASSWD**: BCx38Gbt6qY}=F{fYA&{X

and can be modified on project's Django admin page (<host>/admin/).

On teste-backend/app/cities/functional_tests directory:
```bash
py.test -v
```
will execute all tests.

To execute a single test:
```bash
py.test test_get_city_by_name.tavern.yaml
```

The result will be something like this (all tests):
```bash
=============================================================================== test session starts ===============================================================================
platform linux -- Python 3.7.0, pytest-3.8.1, py-1.6.0, pluggy-0.7.1 -- /home/armando/.virtualenvs/cities/bin/python
cachedir: .pytest_cache
rootdir: /home/armando/Desenv/Python3/Teste/teste-backend/app/cities/functional_tests, inifile:
plugins: tavern-0.18.3
collected 4 items

test_get_all_states.tavern.yaml::Retrieve all union states PASSED                                                                                                           [ 25%]
test_get_city_by_id.tavern.yaml::Retrieve specific city by id PASSED                                                                                                        [ 50%]
test_get_city_by_name.tavern.yaml::Retrieve specific city by name and uf PASSED                                                                                             [ 75%]
test_get_state_by_name.tavern.yaml::Retrieve specific state by name or uf PASSED                                                                                            [100%]
====================================================================== 4 passed, 0 warnings in 2.47 seconds =======================================================================
```

If you are using Python 3.7+ there can be some DeprecationWarning warnings, as some libraries like pyYAML still use an import construction that is deprecated with this version of Python onwards (not reproduced here for sake of clearness).

### Tests descriptions:

#### test_get_all_states.tavern.yaml:

This test retrieves all available union states and respective ids and UFs.


#### test_get_city_by_id.tavern.yaml:

This test:

- Tries to retrive a city with an invalid id (resulting in HTTP 404).
- Creates a new city on a state.
- Retrieves the newly created city.
- Deletes the created city.


#### test_get_city_by_name.tavern.yaml:

This test:

- Creates three new cities, two on the same state and the tird on a different state (the three cities names start by `São`).
- Retrieves a city by exact name and uf.
- Retrieves a city by exact name (case insensitive) and uf.
- Retrives some cities whose name contains `São` as a search term (resulting on the two cities that belongs to the state searched for).
- Retrives some cities whose name contains (case insensitive) `são` as a search term (resulting on the only city that belongs to the state searched for).
- Tries to retrive a city that doesn't belongs to the searched state (resulting on an empty list).
- Tries to create a city that already exists for that uf (resulting in HTTP 400).
- Tries to retrive a city with an invalid search type (resulting in HTTP 400).
- Tries to retrive a city with an inexistent UF (resulting in HTTP 404).
- Tries to retrive a city without specifying an UF (resulting in HTTP 400).
- Tries to retrive a city without specifying a city name (resulting in HTTP 400).
- Tries to retrive a city by exact name with spurious characters in it and uf (the spurious characters are ignored).


#### test_get_state_by_name.tavern.yaml:

This test:

- Retrives a state by name and uf.
- Retrives a state only by name.
- Retrives a state only by uf.
- Tries to retrive a state without parameters (resulting in HTTP 400).
- Tries to retrive a state with an invalid parameter.
- Tries to retrive a state with an inexistent parameter.
