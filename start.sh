#!/bin/bash

python3 manage.py makemigrations cities
python3 manage.py migrate
python3 manage.py loaddata cities/fixtures/init_admin.json
python3 manage.py loaddata cities/fixtures/init_states.json
python3 manage.py runserver 0.0.0.0:8000
