FROM python:3.6

ENV PYTHONUNBUFFERED 1
RUN mkdir /code

WORKDIR /code

ADD app/requirements.txt /code/
RUN pip install -r requirements.txt

ADD app/ /code/

RUN mkdir /start
ADD start.sh /start/start.sh